#include<iostream>
#include <typeinfo>
using std::cout;
using std::bad_cast;


class A {
	public:
	virtual ~A() {}
};

class B : public A {};
class C : virtual public B {};
class D : virtual public B {};
class E : public C, public D {};

char F(A* p, C& r) {
	B* punt = dynamic_cast<B*>(p);
	
	try{
		E& s = dynamic_cast<E&>(r);
	}
	catch(bad_cast) {
		if(punt) return 'O';
		else return 'M';
	}
	if(punt) return 'R';
	else return 'A';
}

int main() {
	A a; B b; C c; D d; E e;
	cout << F(&b,e) << F(&b,c) << F(&a,c) << F(&a,e); 	//STAMPA ROMA
	//R se TD(p) <= TD(B*) && TD(r) <= E&   ----> p qualsiasi tipo <=B, e
	//O
	//M
	//A
}