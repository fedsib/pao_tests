//PROGRAMMA QUATTRO
#include<iostream>
using std::cout;

class A {
	private:
	int z;
	
	public:
	~A() { cout << "Ad ";}
};

class B {
	public:
	A* p;
	A a;
	~B() { cout << "Bd ";}
};

class C { 
	public:
	static B s;
	int k;
	A a;
	~C() { cout << "Cd ";}
};

B C::s = B();

int main(){
	C c1, c2;
	/*
	costruisco lo static B con B() che non c'è, costruisco l'A con A() che non c'è e fa un int
	c1 invoco c() che non c'è,
	costruisco k 
	costruisco a con A() che non c'è e fa un int
	
	costruisco k int
	costruisco A con A() che non c'è e fa un int
	
	distruggo c2, distruggo c1, distruggo s
	STAMPA:
	Bd Ad //distrutto il B temporaneo anonimo usato per la creazione del B statico
	Cd Ad //distrutto c2
	Cd Ad //distrutto c1
	Bd Ad //distrutto B statico
	*/
}