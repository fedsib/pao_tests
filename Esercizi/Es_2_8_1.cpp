#include<iostream>
using std::cout;
using std::endl;
using std::cin;

class C {
	private:
	int x;
	
	public:
	C(){cout << "C0 "; x = 0;}
	C(int k) {cout << "C1 "; x=k;}
};

class D {
	private:
	C c;
	public:
	D(){ cout << "D0 "; c = C(3);}
};

class E {
	private:
	char c;
	C c1;
	
	public:
	D d;
	C c2;
};

int main(){
	char ch;
	D y; cout << endl;			//stampa C0 D0 C1
	E x; cout << endl;			//stampa C0 C0 D0 C1 C0
	E* p = &x; cout << endl;	//stampa niente
	D& a = y; cout << endl;		//stampa niente
	cin >> ch;
}