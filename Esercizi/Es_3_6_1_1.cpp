//PROGRAMMA1
#include<iostream>
using std::cin;
using std::cout;
using std::string;

class C {
	public:
	string s;
	C(string x = "1") : s(x) {}
	~C() { cout << s << "Cd ";}
};

C F(C p) { return p;} 	//funzione esterna, passaggio del parametro per valore

C w("3"); 				//variabile globale

class D {
	public:
	static C c;			//campo dati statico
};

C D::c("4");

int main(){
	cout << "PROGRAMMA UNO\n";
	C x("5"), y("6"); D d;
	y = F(x); cout << "uno\n";
	C z = F(x); cout << "due\n";
	/*STAMPA 
		5Cd 5Cd uno
		5Cd 5Cd due
		5Cd 5Cd 5Cd 4Cd 3Cd
	*/
}