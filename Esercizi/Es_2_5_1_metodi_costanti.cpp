class C {
	private:
		int x;
	public:
		C(int n=0){ x=n; }
		C F (C obj) {C r; r.x = obj.x + x; return r; }
		C G (C obj) const { C r; r.x = obj.x + x; return r; }
		C H (C& obj) { obj.x += x; return obj; }
		C I (const C& obj) { C r; r.x = obj.x + x; return r; }
		C J (const C& obj) const { C r; r.x = obj.x + x; return r; }
};

main(){
	C x, y(1), z(2); const C v(2);
	z = x.F(y);			//corretto, compila
	v.F(y);				//non compila, v è costante ma viene invocato il metodo non costante F sull'oggetto di invocazione v
	v.G(y);				//corretto, v è costante e G è un metodo costante
	(v.G(y)).F(x);		//compila, G ritorna un C che diventa l'oggetto invocazione del metodo non costante F
	(v.G(y)).G(x);		//compila, invoco sul C ritornato un metodo costante. Tutto ok
	x.H(v);				//non compila, v è costante e viene passato per riferimento non costante al metodo.
	x.H(z.G(y));		//non compila, viene passato per riferimento un valore non indirizzabile http://stackoverflow.com/a/20247573
	x.I(z.G(y));		//compila G su z ritorna un C che viene usato come parametro per I
	x.J(z.G(y));		//compila, come sopra. J è un metodo costante e non fa side effects
	v.J(z.G(y));		//compila come sopra. Stavolta però l'oggetto di invocazione è costante
}