class E {
	private:
	int x;
	
	public:
	E(int z=0) : x(z) {}
};

class C {
	private:
	int z;
	
	E x;
	const E e;
	E& r;
	int* const p;
	
	public:
	C() : z(0), x(), e(), r(x), p(&z) {}
};

main(){}