//telefonata.cpp
#include "telefonata.h"

telefonata::telefonata(const orario& i, const orario& f, int n) : inizio(i), fine(f), numero(n) {}

telefonata::telefonata() : numero(0) {} //campi di tipo orario inizializzati col costruttore di default di orario

orario telefonata::Inizio() const { return inizio; }

orario telefonata::Fine() const { return fine; }

int telefonata::Numero() const { return numero; }

bool telefonata::operator==(const telefonata& t) const {
	return inizio == t.inizio && fine == t.fine && numero == t.numero;
}

ostream& operator<<(ostream& o, const telefonata& t) {
	return o << "INIZIO " << t.Inizio() << " FINE " << t.Fine() 
	<< " NUMERO CHIAMATO " << t.Numero();
	
}
