#ifndef DATAORA_H
#define DATAORA_H

class dataora : public orario{
	private:							
	int giorno;
	int mese;
	int anno
	
	public:								
	int Giorno() const;
	int Mese() const;
	int Anno() const;
};

#endif