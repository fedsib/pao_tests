#ifndef DATAORA_H
#define DATAORA_H
class orario{
	private:							//unico campo dati della classe
	int sec;							//scegliamo di rappresentare un orario mediante
										//il numero di secondi trascorsi dalla mezzanotte
	
	public:								//metodi della classe
	orario(int =0, int =0, int =0);		//costruttore a 0,1,2,3 parametri
	
	int Ore();							//selettore delle ore
	int Minuti();						//selettore dei minuti
	int Secondi();						//selettore dei secondi
	
	static orario OraDiPranzo();		//non necessita di oggetto di invocazione, è static
	
	orario operator+(orario);
	orario operator-(orario);
	bool operator==(const orario&);
	bool operator>(const orario&);
	bool operator<(const orario&);
};
#endif