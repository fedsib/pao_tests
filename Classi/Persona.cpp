#include "Persona.h"
#include<iostream>

using namespace std; 

Persona::Persona(string s="Personaggio inventato", int yb = 0, int yd=-1) : nome(s), yob(yb), yod(yd){}	//yd = -1 il personaggio è vivo
	Persona::Persona(const Persona& p){ 
		nome = p.nome;
		yob = p.yob;
		yod = p.yod;
	}
	
	string Persona::getNome() const{
		return nome;
	}
	int Persona::getYob() const{
		return yob;
	}
	int Persona::getYod() const{
		return yod;
	}
	
	ostream& operator<<(ostream& o, const Persona& p){
		return o << "Nome e cognome: " << p.Persona::getNome() << " Nato/a nel " << p.Persona::getYob() 
		<< " Morto/a nel " << p.Persona::getYod();
	}

main(){
	Persona* Pippo_Baudo = new Persona("Pippo Baudo", 1950, -1);
	Persona* Giulio_Cesare = new Persona("Giulio Cesare", 0, 35);
	Persona* Sandra_Mondaini = new Persona("Sandra Mondaini", 1935, 2010);
	char c;
	
	cout << *Pippo_Baudo  << endl;
	cout << *Giulio_Cesare  << endl;
	cout << *Sandra_Mondaini  << endl;
	
	cin >> c;
}