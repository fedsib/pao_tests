#include<iostream>
#include "orario.h"

using namespace std;

orario::orario(int o, int m, int s){
	if(o < 0 || o > 23 || m<0 || m>59 || s < 0 || s>59)
		sec = 0;
	else sec = o*3600 + m*60 + s;
}

int orario::Ore(){ return sec / 3600; }
int orario::Minuti(){ return (sec / 60) % 60; }
int orario::Secondi(){ return sec % 60; }

orario orario::OraDiPranzo() { return orario(13,15); }

orario orario::operator+(orario o){
	orario aux;
	aux.sec = (sec + o.sec) % 86400;
	return aux;
}

orario orario::operator-(orario o){
	orario aux;
	aux.sec = sec - o.sec; 
	if(aux.sec <0)
		aux.sec =0;
	aux.sec = aux.sec % 86400;
	return aux;
}

bool orario::operator==(const orario& o){
	if(sec == o.sec)
		return true;
	return false;
}

bool orario::operator>(const orario& o){
	if(sec > o.sec)
		return true;
	return false;
}

bool orario::operator<(const orario& o){
	if(sec < o.sec)
		return true;
	return false;
}

int main(){
	orario mezzanotte;
	cout << mezzanotte.Secondi() << endl;
	orario le_undici(11);
	orario le_undiici(11);
	orario ventitre(23);
	orario somma = le_undici + le_undiici;
	orario sottrai = ventitre - le_undici;
	orario meno = le_undici - le_undiici;
	
	cout << (le_undici == le_undiici);
}

