//Point.cpp

#include<iostream>
#include "Point.h"
#include<math.h>

using std::cout;
using std::cin;
using std::endl;

/*Point::Point(){
	x = 0;
	y = 0;
	z = 0;
}
*/
Point::Point(double a, double b, double c): x(a), y(b), z(c) {}

double Point::getX() const{
	return x;
}
double Point::getY() const{
	return y;
}
double Point::getZ() const{
	return z;
}

void Point::negate(){
	x = -x;
	y = -y;
	z = -z;
}

double Point::norm() const{
	return sqrt(x*x+y*y+z*z);
}

void Point::print() const {
	cout << "x = " << this->Point::getX() << " y = " << this->Point::getY() 
	<< " z = " << this->Point::getZ() << endl;
}

main(){
	
	Point* punto = new Point(5,4,3);
	
	char c;
	
	punto->print();
	
	cout << "Distanza dall'origine " << punto->norm() << endl;
	
	punto->negate();
	
	punto->print();
	
	cin >> c;
}

