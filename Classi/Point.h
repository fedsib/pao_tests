//Point.h definisce un punto nello spazio tridimensionale
#ifndef POINT_H
#define POINT_H

class Point{
	private:
	double x,y,z;
	
	public:
	//Point();
	Point(double a=0, double b=0, double c=0);
	
	double getX() const;
	double getY() const;
	double getZ() const;
	
	void negate();
	double norm() const;
	void print() const;
	
	
};

#endif