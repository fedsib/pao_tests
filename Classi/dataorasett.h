#ifndef DATAORASETT_H
#define DATAORASETT_H

enum settimana {lun,mar,mer,gio,ven,sab,dom}

class dataorasett : public dataora{
	private:							
	settimana giornosettimana;
	
	public:								
	settimana GiornoSettimana() const;
};

#endif