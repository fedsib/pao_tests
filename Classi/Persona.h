#ifndef PERSONA_H
#define PERSONA_H

#include<iostream>
using namespace std;

class Persona{
	
	private:
	string nome;
	int yob, yod;
	
	public:
	Persona();
	Persona(string s, int yb, int yd);	//yd = -1 il personaggio è vivo
	Persona(const Persona&);
	
	string getNome() const;
	int getYob() const;
	int getYod() const;
};

ostream& operator<<(ostream& o, const Persona& p);

#endif