#include<iostream>
using std::cout;
using std::endl;

template<int I> //parametro valore
class C{
	static int numero;
	public:
	  C();
	  void stampa_numero();
};

//inizializzazione parametrica del campo dati statico
template<int I> int C<I>::numero =I;

template<int I> C<I>::C(){numero++;}

template<int I> void C<I>::stampa_numero(){
	cout << "Valore statico: " << numero << endl;
} 

int main(){
	C<1> uno;
	C<2> due_a, due_b;
	uno.stampa_numero();
	due_a.stampa_numero();
	due_b.stampa_numero();
}