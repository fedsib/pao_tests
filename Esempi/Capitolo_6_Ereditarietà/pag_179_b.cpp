//Ereditarietà e amicizia
#include<iostream>
using std::cout;
using std::endl;

class C {
	private:
	int i;
	
	public:
	C() : i(1) {}
	friend void print(C);
};

void print(C x) {
	cout << x.i << endl;
}

class D : public C {
	private:
	double z;
	
	public:
	D() : z(3.14) {}
	friend void print(D);
};

void print(D x) {
	cout << x.z << endl;
}

int main() {
	C c;
	D d;
	
	print(c);
	print(d);
}