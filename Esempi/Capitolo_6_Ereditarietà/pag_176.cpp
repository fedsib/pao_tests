class C {
	private:
	int priv;
	
	protected:
	int prot;
	
	public:
	int publ;
};

class D : private C {
	//prot e publ divengono qui privati
};

class E : protected C {
	//prot e publ divengono qui protetti
};

class F : public D {
	//prot e publ sono qui inaccessibili
	public:
	void fF(int i, int j){
		prot = i;	//Illegale
		publ = j;	//Illegale
	}
};

class G : public E {
	//prot e publ rimangono qui protetti
	void fG(int i, int j){
		prot = i;	//OK
		publ = j;	//OK
	}
};

int main(){}
