//sul significato di protected
#include<iostream>
using std::cout;
using std::endl;

class B {
	protected:
	int i;
	void protected_printB() const { cout << ' ' << i; }
	
	public:
	void printB() { cout << ' ' << i; }
};

class D : public B {
	private:
	double z;
	
	public:
	static void stampa(const B& b, const D& d) {
		cout << ' ' << b.i; 	// Illegale: "B::i is protected in this context"
		b.printB();				// OK
		b.protected_printB();	// Illegale: "B::protected_printB() is protected in this context"
		
		cout << ' ' << d.i;		// OK
		d.printB();				// OK
		d.protected_printB(); 	// OK
	}
};

int main(){}