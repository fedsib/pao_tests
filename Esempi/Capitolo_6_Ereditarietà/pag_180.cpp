//Conversioni esplicite
#include<iostream>
using std::cout;
using std::endl;

class C {
	public:
	int x;
	void f() { x = 4; }
};

class D : public C {
	public:
	int y;
	void g() { 
		x = 5; 
		y = 6; 
	}
};

class E : public D {
	public:
	int z;
	void h() {
		x = 7;
		y = 8;
		z = 9;
	}
};

/* VI significa Valore (intero) Imprevedibile */

int main() {
	C c; D d; E e;
	
	c.f(); d.g(); e.h();
	
	//PERICOLOSO
	D* pd = static_cast<D*> (&c);
	
	//errore run-time o stampa: 4 VI
	cout << pd->x << " " << pd->y << endl;
	
	//PERICOLOSO
	E& re = static_cast<E&> (d);
	
	//errore run-time o stampa: 5 6 VI
	cout << re.x << " " << re.y << " " << re.z << endl;
	
	C* pc = &d;
	pd = static_cast<D*> (pc);							// OK
	cout << pd->x << " " << pd->y << endl;				// stampa: 5 6
	
	D& rd = e; 
	E& s = static_cast<E&> (rd);						// OK
	cout << s.x << " " << s.y << " " << s.z << endl;
	
}