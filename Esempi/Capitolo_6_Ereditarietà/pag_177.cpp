//sul significato di inaccessibile

#include<iostream>
using std::cout;
using std::endl;

class C {
	private:
	int i;
	
	public:
	C() : i(1) {}
	void print() { cout << ' ' << i; }
};

class D : public C {
	private:
	double z;
	
	public:
	D() : z(3.14) {}
	void print() {
		C::print();					//l'oggetto di invocazione di C::print() è il sottoggetto di tipo C dell'oggetto d'invocazione
		cout << ' ' << z;
	}
};

int main(){
	C c; 
	D d;
	
	c.print(); cout << endl;
	d.print(); cout << endl;
}