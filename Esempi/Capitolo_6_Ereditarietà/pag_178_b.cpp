//Ereditarietà e amicizia
#include<iostream>
using std::cout;
using std::endl;

class C {
	private:
	int i;
	
	public:
	C() : i(1) {}
	friend void print(C);
};

class D : public C {
	private:
	double z;
	
	public:
	D() : z(3.14) {}
};

void print(C x) {
	cout << x.i << endl;
	D d;
	
	cout << d.z;  	// Illegale: "z is private within this context"
}

int main() {
	C c;
	D d;
	print(c);
	print(d);
}