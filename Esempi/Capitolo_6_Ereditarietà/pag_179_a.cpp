//Ereditarietà e amicizia
#include<iostream>
using std::cout;
using std::endl;

class C {
	friend class Z;
	
	private:
	int i;
	
	public:
	C() : i(1) {}
	friend void print(C);
};

class D : public C {
	private:
	double z;
	
	public:
	D() : z(3.14) {}
};

class Z {
	public:
	void m() {
		C c;
		D d;
		cout << c.i;	// OK
		cout << d.z; 	// Illegale: "z is private within this context"
	}
};

int main() {
	Z z;
	z.m();
}