class C {
	private:
	int i;
	
	protected:
	char c;
	
	public:
	float f;
};

class D : private C {};		//derivazione privata
class E : protected C {}; 	//derivazione protetta

int main(){
	C c, *pc; 
	D d, *pd;
	E e, *pe;
	
	c = d;		// Illegale
	c = e;		// Illegale
	pc = &d;	// Illegale
	pc = &e;	// Illegale
	C& rc = d;	// Illegale
	
}