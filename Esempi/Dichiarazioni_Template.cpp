//Esempio dichiarazione nel template di classe C di una classe o funzione friend non template

class A { int fun(); };

template<class T> class C {
	friend int A::fun();
	friend class B;
	friend bool test;
};
//#fine esempio

//Esempio dichiarazione nel template di classe C di un template di classe o di un template di funzione associato

template<class T> class A { int fun(); };

template<class T> class B {};

//dichiarazione incompleta del template di classe C 
template<class T1, class T2> class C;

//dichiarazione del template di funzione test associato a C
template<class T1, class T2> bool test(C<T1,T2>);

//definizione del template di classe C

template<class T1, class T2> class C {
	friend int A<T1>::fun();
	friend class B<T2>;
	friend bool test<T1,T2>(C);
};

//#fine esempio2

//dichiarazione nel template di classe C di un template di classe o di un template di funzione non associato
template<class T> class C{
	T t;
	template<class Tp> friend int A<Tp>::fun();
	template<class Tp> friend class B;			//perché non viene istanziato esplicitamente usando template<class Tp> friend class B<Tp>?	
	template<class Tp> friend bool test(C<Tp>); //perché  viene istanziato esplicitamente il template di classe C parametro della funzione,
												//invece del template di funzione test, come in riga 29?
};