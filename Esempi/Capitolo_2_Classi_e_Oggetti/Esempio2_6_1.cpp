class C{
	int dato;
	public:
	C(int);				//costruttore ad un argomento
	static int cont;	
};

int C::cont =0;

C::C(int n){ cont++; dato =n; }

#include<iostream>
using std::cout;
int main(){
	C c1(1), c2(2);
	cout << C::cont;
}