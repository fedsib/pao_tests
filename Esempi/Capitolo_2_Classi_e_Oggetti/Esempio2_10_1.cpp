#include<iostream>
using std::cin;
using std::cout;
using std::endl;


class D {
	public:
	D() { cout << "D0 ";}
	D(int a) { cout << "D1 ";}
	//D(const D& d) { cout << " Dc ";}
};

class E {
	private:
	D d;
	
	public:
	E() : d(3) { cout << "E0 ";}
	E(double a, int b) { cout << "E2 ";}
	E(const E& a) : d(a.d) { cout << "Ec ";}
};

class C {
	private:
	int z;
	E e;
	D d;
	E* p;
	
	public:
	C() : p(0), e(), z(4) { cout << "C0 ";}
	C(int a, E b) : e(3.72,2), p(&b), z(1), d(3) { cout << "C1 ";}
	C(char a, int b) : e(), d(2), p(&e) { cout << "C2 ";}
};

int main(){
	char a;
	E e; cout << endl;
	C c; cout << endl;
	C c1(1,e); cout << endl;
	C c2('b',2); cout << endl;
	cin >> a;
}