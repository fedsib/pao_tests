#include"AlbBinRic.h"
template<class T> Nodo<T>* AlbBinRic<T>::Find(T v) const{
	return FindRic(radice,v);
}

template<class T> Nodo<T>* AlbBinRic<T>::FindRic(Nodo<T>* x, T v){
	if(!x) 				//caso base albero vuoto
		return x;
	if(x->info == v)	//caso base, v è nella radice
		return x;
	if(v < x->info) 	//cerca ricorsivamente nel sottoalbero sinistro
		return FindRic(x->sinistro,v);
	else				//cerca ricorsivamente nel sottoalbero destro
		return FindRic(x->destro,v);
}

template<class T> Nodo<T>* AlbBinRic<T>::Massimo() const {
	if(!radice) return 0;
	return MassimoRic(radice);
}

template<class T> Nodo<T>* AlbBinRic<T>::MassimoRic(Nodo<T>* x){
	if(!(x->destro)) return x;
	else return MassimoRic(x->destro);
}

template<class T> Nodo<T>* AlbBinRic<T>::Minimo() const {
	if(!radice) return 0;
	return MinimoRic(radice);
}

template<class T> Nodo<T>* AlbBinRic<T>::MinimoRic(Nodo<T>* x){
	if(!(x->sinistro)) return x;
	else return MassimoRic(x->sinistro);
}

template<class T> Nodo<T>* AlbBinRic<T>::Succ(Nodo<T>* x) const {
	if(!x) return 0;
	if(x->destro) return MassimoRic(x->destro);
	while(x->padre && x->padre->destro ==x) 
		x = x->padre;
	return x->padre;
}

template<class T> Nodo<T>* AlbBinRic<T>::Pred(Nodo<T>* x) const {
	if(!x) return 0;
	if(x->sinistro) return MassimoRic(x->sinistro);
	while(x->padre && x->padre->sinistro ==x) 
		x = x->padre;
	return x->padre;
}

template<class T> void AlbBinRic<T>::Insert(T v){
	if(!radice)
		radice = new Nodo<T>(v);
	else
		InsertRic(radice,v);
}

template<class T> void AlbBinRic<T>::InsertRic(Nodo<T> x, T v){
	if(v < x->info)
		if(x->sinistro == 0) 
			x->sinistro = new Nodo<T>(v,x);
		else
			InsertRic(x->sinistro,v);
	else 
		if(x->destro == 0)
			x->destro = new Nodo<T>(v,x);
		else 
			InsertRic(x->destro,v);
}