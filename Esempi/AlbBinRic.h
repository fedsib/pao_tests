#ifndef ALBBINRIC_H
#define ALBBINRIC_H
#include<iostream>
using std::ostream;

template<class T> class AlbBinRic;

template<class T> ostream& operator<< (ostream&, const AlbBinRic<T>&);

template<class T> class AlbBinRic{
	friend ostream& operator<< <T>(ostream&, const AlbBinRic<T>&);
	
	private:
		Nodo<T>* radice;
		static Nodo<T>* FindRic(Nodo<T>*, T);
		static Nodo<T>* MinimoRic(Nodo<T>*);
		static Nodo<T>* MassimoRic(Nodo<T>*);
		static void InsertRic(Nodo<T>*,T);
	
	public:
		AlbBinRic() : radice(0){}
		Nodo<T>* Find(T) const;
		Nodo<T>* Minimo() const;
		Nodo<T>* Massimo() const;
		Nodo<T>* Succ(Nodo<T>*) const;
		Nodo<T>* Pred(Nodo<T>*) const;
		void Insert(T);
		static T Valore(Nodo<T>* p){ return p->info;}
};
#endif