#include<iostream>
#include "nodo.h"
#include "AlbBinRic.h"
using std::cout;
using std::endl;

int main(){
	AlbBinRic<int> a;
	a.Insert(3); a.Insert(2);
	a.Insert(3); a.Insert(1);
	a.Insert(6); a.Insert(5);
	cout << a;
	
	cout << "minimo: " << AlbBinRic<int>::Valore(a.Minimo()) << endl;
	cout << "massimo: " << AlbBinRic<int>::Valore(a.Massimo()) << endl;
	
	cout << "successore minimo: " << AlbBinRic<int>::Valore(a.Succ(a.Minimo())) << endl;
	cout << "predecessore massimo: " << AlbBinRic<int>::Valore(a.Pred(a.Massimo())) << endl;
	
	if(a.Find(2)) 
		cout << "Il valore " << 2 << " è presente\n";
}