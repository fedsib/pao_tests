#ifndef QUEUE_H
#define QUEUE_H

#include<iostream>
#include <cstdlib>

using std::cerr; 
using std::endl;
using std::ostream;

template <class T> class Queue;

template <class T>
class QueueItem {
	friend class Queue<T>; //dichiarazione d'amicizia associata
	friend ostream& operator<< <T>(ostream&, const Queue<T>&);	
	friend ostream& operator<< <T>(ostream&, const QueueItem<T>&);	
	private:
	T info;
	QueueItem* next;
	QueueItem(const T& val) : info(val), next(0) {}
};

template <class T>
class Queue {
	friend ostream& operator<< <T>(ostream&, const Queue<T>&);	

	public:
	Queue() : primo(0), ultimo(0){};
	bool is_empty() const;
	void add(const T&);
	T remove();
	~Queue();
	
	private:
	static int contatore;
	QueueItem<T>* primo;
	QueueItem<T>* ultimo;
};

//definizioni

template<class T> int Queue<T>::contatore = 0;
template <class T>
bool Queue<T>::is_empty() const {
	return (primo == 0);
}

template <class T>
void Queue<T>::add(const T& val) {
	QueueItem<T>* p = new QueueItem<T>(val);
	if(is_empty())
		primo = ultimo = p;
	else { //aggiunge in coda
		ultimo->next = p;
		ultimo = p;
	}	
}

template <class T>
T Queue<T>::remove(){
	if(is_empty()){
		cerr << "remove() su coda vuota" << endl;
		exit(1);
	}
	QueueItem<T>* p = primo;
	primo = primo->next;
	T aux = p->info;
	delete p;
	return aux;
}

template <class T>
Queue<T>::~Queue(){
	while(!(is_empty()))
		remove();
}

template<class T> 
ostream& operator<<(ostream& os, const QueueItem<T>& qi){
	os << qi.info; //amicizia con QueueItem, notare che si richiede operator<< per il tipo T
	return os;
}

template<class T> 
ostream& operator<<(ostream& os, const Queue<T>& q){
	os << "(";
	QueueItem<T>* p = q.primo;	//per amicizia con Queue
	for(; p!=0;p=p->next)		//per amicizia con QueueItem
		os << *p << " ";
	os << ")" << endl;
	return os;
}

#endif