#ifndef NODO_H
#define NODO_H
#include<iostream>
using std::ostream;
template<class T> class Nodo;
template<class T> class AlbBinRic;

template<class T> ostream& operator<<(ostream&, Nodo<T>*);

template<class T> class Nodo{
	friend class AlbBinRic<T>; 	//classe associata
	friend ostream& operator<< <T>(ostream&, Nodo<T>*);
	
	private:
	T info;
	Nodo *sinistro, *destro, *padre;
	Nodo(const T& v, Nodo* p=0, Nodo* s=0, Nodo* d=0) : info(v), padre(p), sinistro(s), destro(d){}
};

template<class T> ostream& operator<< (ostream& os, Nodo<T>* p){
	if(!p) 
		os << '@'; //caso base
	else 
		os << "(" << p->info << "," << p->sinistro << "," << p->destro << ")"; 
	return os;
}
#endif