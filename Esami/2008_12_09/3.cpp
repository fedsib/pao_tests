#include<iostream>
using std::cout;

class D;

class B {
	public:
	virtual D* f() = 0;
};

class C {
	public:
	virtual C* g();
	virtual B* h()=0;
};

class D : public B, public C {
	public:
	D* f() { cout << "D::f "; return new D; }
	D* h() { cout << "D::h "; return dynamic_cast<D*>(g()); }
};

C* C::g() {
	cout << "C::g ";
	B* p = dynamic_cast<B*>(this);
	if(p) return p->f(); else return this;
}

class E : public D {
	E* f() {
		cout << "E::f ";
		E* p = dynamic_cast<E*>(g());
		if(p) return p; else return this;
	}
};

class F : public E {
	public:
	E* g() { cout << "F::g "; return new F; }
	E* h() { 
		cout << "H::f "; 
		E* p = dynamic_cast<E*>(E::g());
		if(p) return p; else return new F;
	}
};

main () {
	B * p; C * q; D* r;
	//p = new E; p->h();						NON COMPILA, B non ha una funzione h()
	//p = new E; p->f();						ERRORE RUN TIME f definita in E e g definita in C si richiamano l'un l'altra all'infinito
	//p = new D; (dynamic_cast<D*>(p))->h();	STAMPA D::h C::g D::f
	//q = new D; q->g();						STAMPA C::g D::f
	//q = new E; q->h();						ERRORE RUN TIME f definita in E e g definita in C si richiamano l'un l'altra all'infinito
	//q = new F; q->g();						STAMPA F::g
	//r = new E; r->f();						ERRORE RUN TIME f definita in E e g definita in C si richiamano l'un l'altra all'infinito
	//r = new F; r->f();						STAMPA E::f F::g perché g è ridefinita in F
	//r = new F; r->g();						STAMPA F::g
	//r = new F; r->h();						STAMPA H::f C::g E::f F::g
	
}