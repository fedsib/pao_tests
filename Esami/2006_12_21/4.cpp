#include<iostream>
#include<typeinfo>
using std::cout;
using std::bad_cast;

class A {
	public:
	virtual ~A(){};
};

class B : public A {};

class C : public A {};

class D : virtual public B {};

class E : virtual public B {};

class F : public D, public E {};

void f(A* p) throw(E*){
	if(!dynamic_cast<D*>(p)) throw new E();
}

char fun(A* p, B& r) {
	if(typeid(*p)==typeid(r)) return '1';
	try {
		f(&r);
	} catch(B*) {return '2';}
	
	E* punt = dynamic_cast<F*>(&r);
	try{
		B& ref = dynamic_cast<B&>(*p);
	}
	catch(bad_cast){
		if(!punt) return '3';
		return '4';
	}
	if(punt) return '5';
	return '6';
}

main(){
	A a; B b; C c; D d; E e; F f;
	cout << fun(&b,b) << fun(&a,b) << fun(&a,d) << fun(&a,f) << fun(&b,f) << fun(&b,d);
}