#include<iostream>
using namespace std;

class C {
	
	public:
	int a[2];
	C(int x = 0, int y=1) { a[0]=x; a[1] = y; cout << "C(" << a[0] << "," << a[1] << ") "; }
	C(const C& c) { cout << "Cc ";}
};

class D {
	private:
	C c1;
	C *c2;
	C& cr;
	
	public:
	D() : c2(&c1), cr(c1) { cout << "D() "; }
	D(const D&) : cr(c1) { cout << "Dc "; }
	~D() { cout << "~D ";}
};

class E {
	public:
	static C cs;
};

C E::cs = 1;

int main() {
	C c; cout << "UNO" <<endl;										//STAMPA C(1,1) Cc C(0,1) UNO
	C x(c); cout << x.a[0] << " " << x.a[1] << " DUE" << endl;		//STAMPA Cc ValoreCasuale ValoreCasuale DUE x è costruito di copia da c, ma essendo il costruttore di copia di C ridefinito perde la facoltà di copiare campo dato per campo dato
	D d = D(); cout << "TRE" << endl;								//STAMPA C(0,1) D() C(0,1) Dc ~D TRE, è una costruzione di copia. 
																	//Prima si invoca il costruttore di default di D che va a costruire c1 usando il costruttore di C
																	//poi esegue il corpo del costruttore di D, costruisce c1 tramite l'invocazione esplicita
																	//del costruttore di default C nel costruttore di copia di D, esegue il corpo del costruttore di D e rimuove il D utilizzato per la copia.
	E e; cout << "QUATTRO" << endl;									//STAMPA QUATTRO, l'unico campo dati di E è lo static C costruito prima
	return 0;														//STAMPA ~D
}