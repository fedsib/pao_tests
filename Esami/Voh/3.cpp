namespace ns {
	class C {
		public:
		C(int n=0) : x(n) {}
		
		private:
		friend int f();
		int x;
	};
}

int f() {
	ns::C c;
	return c.x;
}

int main() {
	f();
	return 0;
}

//NON COMPILA, f non può accedere alla parte privata di C in quanto è definita all'esterno del namespace