#include<iostream>
using namespace std;

class C {
	private:
	int number;
	
	public:
	C(int n = 1) : number(n) { cout << "C(" << number << ") "; }
	~C() { cout << "~C(" << number << ") ";; }
	C& operator=(const C& c) { number = c.number; cout << "operator(" << number << ") "; }
};

int F(C c) { return c.number; }

main(){}