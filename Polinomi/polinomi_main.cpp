// File polinomi_main.cpp

#include "polinomio.h"
#include <iostream>
using std::cout; using std::endl;

int main() {
	Polinomio X(1,1);
	cout<< "X = "<< X << endl; //stampa: X = x

	Polinomio Z(3,4);
	cout<< "Z = "<< Z << endl; //stampa: Z = 3x^4

	Polinomio T(X);
	cout<< "T = "<< T << endl; //stampa: T = x

	T=Z;
	cout<< "T = "<< T << endl; //stampa: T = 3x^4
	cout<< "X = "<< X << endl; //stampa: X = x

	Polinomio Q=3*X + 2*X*X - X*X*X + 7*(X^4);
	cout<< "Q = "<< Q << endl;

	Polinomio P = 2*(X^2);
	cout<< "P = "<< P << endl;

	cout<< "Q*Q = "<< Q*Q << endl;

	cout<<"Q/P = "<< Q/P << endl;

	cout<<"Q^3 = "<< (Q^3) << endl;

	cout<<"Q%P = "<< Q%P << endl;

	cout<< "Q(3) = " << Q(3) << endl;

	if (P == 2*(X^2)) cout << "P(3) = " <<P(3) << endl;

	if (P!=Q) cout << "Q(P(3)) = "<< Q(P(3)) << endl;

}