//File polinomio.cpp

#include "polinomio.h"
#include <iostream>
#include <math.h>

using std::ostream; 
using std::endl; 
using std::cout;

//*****SMARTP*****

//Costruttore a 0-1 args + convertitore di tipo
Polinomio::SmartP::SmartP(Monomio* p) : punt(p) {
	if (punt)
		punt->riferimenti++;
}

//Costruttore di copia
Polinomio::SmartP::SmartP(const SmartP& s) : punt(s.punt) {
	if (punt)
		punt->riferimenti++;
}

//Distruttore
Polinomio::SmartP::~SmartP() {
	if (punt) {
		punt->riferimenti--;
		if (punt->riferimenti==0)
			delete punt; //richiama la delete standard di Monomio
	}
}

//Assegnazione
Polinomio::SmartP& Polinomio::SmartP::operator=(const SmartP& s) {
	if (this != &s) {
		Monomio* t=punt;
		
		punt=s.punt;
		if (punt)
			punt->riferimenti++;

		if (t) {
			t->riferimenti--;
			if (t->riferimenti==0)
				delete t;	//richiama la delete standart di monomio;
		}
	}
	return *this;
}

//Dereferenziazione
Polinomio::Monomio& Polinomio::SmartP::operator*() const {
	return *punt;
}

//Accesso a membro
Polinomio::Monomio* Polinomio::SmartP::operator->() const {
	return punt;
}

//Uguaglianza
bool Polinomio::SmartP::operator==(const SmartP& s) const {
	return punt==s.punt;
}

//Disuguaglianza
bool Polinomio::SmartP::operator!=(const SmartP& s) const {
	return punt!=s.punt;
}

//*****MONOMIO*****

//Costruttore a 1,2,3 args
Polinomio::Monomio::Monomio(int coeff, int esp, const SmartP& p) :
	coefficiente(coeff), esponente(esp), riferimenti(0), next(p) {}

Polinomio::Monomio Polinomio::Monomio::operator+(const Monomio& s) const {
	return Monomio(coefficiente+s.coefficiente, esponente);
}

Polinomio::Monomio Polinomio::Monomio::operator-(const Monomio& s) const {
	return Monomio(coefficiente-s.coefficiente, esponente);
}

Polinomio::Monomio Polinomio::Monomio::operator*(const Monomio& s) const {
	return Monomio(coefficiente*s.coefficiente, esponente+s.esponente);
}

//*****POLINOMIO*****

//Costruttore a 0 args
Polinomio::Polinomio() : first(0) {} //coversione di 0 in SmartP

Polinomio::Polinomio(int a, int n) : first( new Monomio(a,n) ) {}

Polinomio::Polinomio(const SmartP& p) : first(p) {}

//PRE=(p e' polinomio corretto)
bool Polinomio::Nullo() const {
	if (first!=0)
		if (first->coefficiente==0 && first->esponente==0)
			return true;
		else
			return false;
//first==0
return true;
}
//POST=(il metodo ritorna true sse il polinomio e' nullo.)

//PRE=(i due polinomi sono ordinati per esponenti decrescenti)
Polinomio operator+(const Polinomio& m,const Polinomio& s) {

	Polinomio::SmartP p1=m.first;	//polinomio 1
	Polinomio::SmartP p2=s.first;	//polinomio 2

	if (p1==0)
		return s;
	else if (p2==0)
		return m;
	//else {

	Polinomio somma;				//Polinomio somma;
	Polinomio::SmartP prec=0;


	while ( (p1!=0) && (p2!=0) ) {
		if (p1->esponente > p2->esponente) {
			Polinomio::SmartP q=Polinomio::SmartP(
						new Polinomio::Monomio(p1->coefficiente, p1->esponente));

			if (prec==0) {
				prec=q;
				somma.first=prec;
			}
			else {
				prec->next=q;
				prec=prec->next; //aggiorno prec;
			}

			p1=p1->next;
		}
		else
		if (p1->esponente < p2->esponente) {
			Polinomio::SmartP q=Polinomio::SmartP(
						new Polinomio::Monomio(p2->coefficiente, p2->esponente));

			if (prec==0) {
				prec=q;
				somma.first=prec;
			}
			else {
				prec->next=q;
				prec=prec->next; //aggiorno prec;
			}
			p2=p2->next;
		}
		else { //devo sommare i monomi
			Polinomio::Monomio temp= (*p1) + (*p2); //+,- tra monomi
			//devo controllare che la somma tra monomi sia != 0
			if (temp.coefficiente !=0) {
				Polinomio::SmartP q=Polinomio::SmartP(
						new Polinomio::Monomio(temp.coefficiente, temp.esponente));
				if (prec==0) {
					prec=q;
					somma.first=prec;
				}
				else {
					prec->next=q;
					prec=prec->next; //aggiorno prec;
				}
			}
			p1=p1->next;
			p2=p2->next;
		}

	} //fine while;

	if (p1==0) {
		if (p2!=0)
			prec->next=p2;
		//else p1==0, p2==0; non devo fare niente;
	}
	else //p1 !=0, p2==0
		prec->next=p1;
	
return somma;
}

Polinomio operator*(const Polinomio& m, const Polinomio& s) {

	Polinomio::SmartP p1=m.first;	//polinomio 1
	Polinomio::SmartP p2=s.first;	//polinomio 2


	if (p1==0 || p2==0)
		return 0;
	//else {

	Polinomio prod;				//Polinomio prodotto; e' il polinomio nullo

	while ( p1!=0) {
		p2=s.first;
		while (p2!=0) {
			Polinomio::Monomio x= (*p1) * (*p2); //* tra monomi
			Polinomio temp(x.coefficiente, x.esponente);
			prod = prod + temp;	//somma tra polinomi;
		p2=p2->next;
		}

		p1=p1->next;
	}	
	return prod;
}


Polinomio operator-(const Polinomio& m,const Polinomio& s) {
	Polinomio temp= s*(-1);
	return (m + temp);
}


Polinomio Polinomio::operator^(int i) const {
	Polinomio aux=1;
	while (i!=0) {
		aux= aux * (*this);
	i--;
	}
	return aux;
}

int Polinomio::operator() (int i) const{
	int aux=0;
	SmartP p=first;
	while (p!=0) {
		aux= aux + p->coefficiente * pow(i,p->esponente);
	p=p->next; 
	}
	return aux;
}

bool operator==(const Polinomio& m, const Polinomio& s) {
	//se ho due polinomi nulli
	if ( (m.Nullo()) && (s.Nullo()) ){
		return true;
	}
	//se solamente uno dei polinomi e' nullo
	else if ( (m.Nullo()) || (s.Nullo()) ) {
		return false;
	}
	else {
		return m.first->coefficiente == s.first->coefficiente  
				&& m.first->esponente == s.first->esponente 
				&& Polinomio(m.first->next) == Polinomio(s.first->next);
				//invocazione ricorsiva su (m,s)->next;
	}
}

bool operator!=(const Polinomio& m, const Polinomio& s) {
	return !(m==s);
}

Polinomio operator/(const Polinomio& m, const Polinomio& s) {
	Polinomio aux;

	//se (almeno) uno dei polinomi e' il polinomio nullo, ritorno 0
	if (m.Nullo() || s.Nullo()) {
		return aux;
	}
	//else

	if (m.first->esponente >= s.first->esponente) {
		aux = Polinomio(m.first->coefficiente/s.first->coefficiente,
									m.first->esponente - s.first->esponente);
		if (aux.first->esponente!=0) {
			aux.first->next = (Polinomio((m - (aux*s)).first->next) / s).first;
		}
	}

	return aux;
}

Polinomio operator%(const Polinomio& m, const Polinomio& s) {
	return m-(m/s)*s;
}

//stampa di un monomio;
ostream& operator<<(ostream& os, const Polinomio::SmartP& p) {
	if ( p->coefficiente>1 || p->coefficiente<-1)
		os << p->coefficiente;
	else
		if (p->coefficiente==-1)
			os << "-";

	if ( p->esponente != 0) {
		os<<"x";
		if (p->esponente != 1) { //>1
			os<<"^"<<p->esponente;
		}
	}
	else //esponente ==0
		if (p->coefficiente==1 || p->coefficiente==-1)
			os<<"1";

return os;
}

//stampa di un polinomio;
ostream& operator<<(ostream& os, const Polinomio& obj) {
	if (obj.Nullo())
		os<<"Polinomio Nullo" << endl;
	else {
		Polinomio::SmartP p = obj.first;	// per amicizia!
		while (p!=0) {	//conversione 0->SmartP
			cout<< p; //stampa il monomio puntato da p;

	if ( p->next!=0 && (p->next)->coefficiente>0)
		cout<< "+";

		p=p->next; //aggiorno p;
		}
	}
	return os;
}