//File polinomio.h

#ifndef POLINOMIO_H
#define POLINOMIO_H
#include<iostream>
using std::ostream;

class Polinomio {
 private:
	class Monomio;	//dichiarazione incompleta
	class SmartP {
	 public:
		Monomio* punt;

		SmartP(Monomio* p=0);	//Costruttore a 0-1 args + convertitore di tipo
		SmartP(const SmartP&);				//Costruttore di copia
		~SmartP();							//Distruttore
		SmartP& operator=(const SmartP&);	//Assegnazione

		Monomio& operator*() const;				//Dereferenziazione
		Monomio* operator->() const;			//Accesso a membro
		bool operator==(const SmartP&) const;	//Uguaglianza
		bool operator!=(const SmartP&) const;	//Disuguaglianza 		
	};

	class Monomio {
	 public:
		int coefficiente, esponente, riferimenti;
		SmartP next;

		Monomio(int ,int =0, const SmartP& =0);

		Monomio operator+(const Monomio&) const;
		Monomio operator-(const Monomio&) const;
		Monomio operator*(const Monomio&) const;
	};

	SmartP first;
	Polinomio(const SmartP&);

 public:
	//Costruttore a 0 args
	Polinomio();
	Polinomio(int, int =0);

	bool Nullo() const;
	friend Polinomio operator+(const Polinomio&,const Polinomio&);
	friend Polinomio operator-(const Polinomio&,const Polinomio&);
	friend Polinomio operator*(const Polinomio&,const Polinomio&);
	Polinomio operator^(int) const;
	int operator() (int) const;
	friend bool operator==(const Polinomio&, const Polinomio&);
	friend bool operator!=(const Polinomio&, const Polinomio&);
	friend Polinomio operator/(const Polinomio&, const Polinomio&);
	friend Polinomio operator%(const Polinomio&, const Polinomio&);	

	friend ostream& operator<<(ostream&, const Polinomio&);
	friend ostream& operator<<(ostream&, const SmartP&);
};
#endif