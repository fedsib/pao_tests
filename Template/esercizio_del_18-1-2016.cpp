// Esame scritto del 17-3-2014

// la classe T deve rendere disponibile operator< e operator>
template<class T>
class dList {

private:
  class nodo {
  public:
    T info;
    nodo* prev;
    nodo* next;
    nodo(const T& t, nodo* p, nodo* n): info(t), prev(p), next(n) {}
    ~nodo() {if(next) delete next;}
  };

  nodo *first, *last; // puntatori al primo ed ultimo nodo della dList
  // dList vuota iff first==last==0

  static bool compareLex(nodo* l1, nodo* l2) {
    if(!l1 && !l2) return false;
    if(!l1 && l2) return true; 
    if(l1 && !l2) return false;
    // tutte e due non vuote
    if(l1->info < l2->info) return true;
    if(l1->info > l2->info) return false;
    /// chiamata ricorsiva
    // l1, l2 non vuote, l1.info == l2.info 
    return compareLex(l1->next,l2->next);
  }

  // ritorna il puntatore first ad una copia della lista
  static nodo* copia(nodo* f) {
    if(!f) return 0;
    nodo* t = new nodo(f->info,0,0);
    nodo* p = t;
    while(f->next) {
      p->next = new nodo(f->next->info,p,0);
      p=p->next;
      f=f->next;
    }
    return t;
  }

  static nodo* getLast(nodo* f) {
    if(!f) return 0;
    while(f->next) f=f->next;
    return f;
  } 

public:
  ~dList() {delete first;}

  dList(const dList& l): first(copia(l.first)), last(getLast(first)) {}

  dList& operator=(const dList& l) {
    if(this != &l) {
      if(first) delete first;
      first = copia(l.first);
      last = getLast(first);
    }
    return *this;
  }

  void insertFront(const T& t) {
    if(first) {
      first = new nodo(t,0,first);
      (first->next)->prev = first;
    }
    else 
      last= first = new nodo(t,0,0);
  }

  void insertBack(const T& t) {
    if(last) {
      last = new nodo(t,last,0);
      (last->prev)->next = last;
    }
    else 
      last= first = new nodo(t,0,0);
  }

  dList(int k, const T& t): first(0), last(0) {
    for(int i=0; i<k; ++k) insertFront(t);
  }

  bool operator<(const dList& l) const {
    return compareLex(first,l.first);
  }
}; 

int main() {
  dList<int> d(4,5), e(d), f(6,9);
  f=e;
  f.insertBack(5); 
  d.insertFront(9);
  d<f;
}