/* ESERCIZIO della lezione del 9/12/2015

Definire una superclasse Auto i cui oggetti rappresentano generiche automobili 
e due sue sottoclassi Benzina e Diesel, i cui oggetti rappresentano automobili 
alimentate, rispettivamente, a benzina e a diesel (ovviamente non esistono 
automobili non alimentate e si assume che ogni auto e` o benzina o diesel). 
Ci interessera` l'aspetto fiscale delle automobili, cioe` il calcolo del 
bollo auto. Queste classi devono soddisfare le seguenti specifiche:

1) Ogni automobile e` caratterizzata dal numero di cavalli fiscali. La tassa per 
cavallo fiscale e` unica per tutte le automobili (sia benzina che diesel) ed e` 
fissata in 5 euro. La classe Auto fornisce un metodo tassa() che ritorna la 
tassa di bollo fiscale per l'automobile di invocazione.

2) La classe Diesel e` dotata (almeno) di un costruttore ad un parametro intero x che 
permette di creare un'auto diesel di x cavalli fiscali. Il calcolo del bollo fiscale 
per un'auto diesel viene fatto nel seguente modo: si moltiplica il numero di cavalli 
fiscali per la tassa per cavallo fiscale e si somma una addizionale fiscale unica 
per ogni automobile diesel fissata in 100 euro.

3) Un'auto benzina puo` soddisfare o meno la normativa europea Euro5. La classe Benzina 
e` dotata di (almeno) un costruttore ad un parametro intero x e ad un parametro 
booleano b che permette di creare un'auto benzina di x cavalli fiscali che soddisfa 
Euro5 se b vale true altrimenti che non soddisfa Euro5. Il calcolo del bollo fiscale 
per un'auto benzina viene fatto nel seguente modo: si moltiplica il numero di cavalli 
fiscali per la tassa per cavallo fiscale; se l'auto soddisfa Euro5 allora si detrae un 
bonus fiscale unico per ogni automobile benzina fissato in 50 euro, altrimenti non vi 
e` alcuna detrazione.

4) Si definisca inoltre una classe ACI i cui oggetti rappresentano delle filiali ACI 
addette all'incasso dei bolli auto. Ogni oggetto ACI e` caratterizzato da un array 
di puntatori ad auto che rappresenta le automobili gestite dalla filiale ACI. La 
classe ACI fornisce un metodo incassaBolli() che ritorna la somma totale dei bolli 
che devono pagare tutte le auto gestite dalla filiale di invocazione.
*/


// Auto e' una classe astratta (e polimorfa)
class Auto {
private:
  unsigned int cf; // numero di cavalli fiscali
public:
  static const double tassaPerCF;
  Auto(int x=1): cf(x) {}
  // metodo virtuale puro
  virtual double tassa() const =0; 
  virtual ~Auto() {} // distruttore standard virtuale
  int getCavalli() const {return cf;}
};
const double Auto::tassaPerCF = 5.0;

class Diesel: public Auto {
public:
  Diesel(int x=1): Auto(x) {}
  static const double addizionaleDiesel;
  // implementazione di tassa()
  double tassa() const {
    return getCavalli()*tassaPerCF + addizionaleDiesel;
  }
};
const double Diesel::addizionaleDiesel = 100.0;

class Benzina: public Auto {
private:
  bool euro5; // true se l'auto benzina e' euro5
public:
  static const double detrazioneEuro5;
  Benzina(int x=1, bool e5=true): Auto(x), euro5(e5) {}
  // implementazione di tassa()
  double tassa() const {
    return euro5 ? 
      getCavalli()*tassaPerCF-detrazioneEuro5 :   
      getCavalli()*tassaPerCF;
  }
};
const double Benzina::detrazioneEuro5 = 50.0;

// utente della gerarchia
class ACI {
private:
  const Auto* * automobili;
  int numeroAuto;
public:
  double incassaBolli() const {
    double somma=0.0;
    for(int k=0; k<numeroAuto; ++k) 
      somma += automobili[k]->tassa();   // chiamata (super)polimorfa
    return somma;
  }
};